const mongoose=require("mongoose");

var SchemaUser=new mongoose.Schema({
    account : {type:String, required:true},
    password: {type:String, required:true, minlength:4},
    recettesTestees: {type:Array, required:true}
});

mongoose.model("User", SchemaUser);