const mongoose=require("mongoose");

var ImagesSchema=new mongoose.Schema({
    url: {type:String},
    image: {data: Buffer, contentType: String}
});

mongoose.model("Images",ImagesSchema);