const express=require("express");
const bodyParser=require("body-parser");
const cors=require("cors");
const mongoose=require("mongoose");
const fs=require("fs"); 
const http=require("http");
const socket=require("socket.io");

mongoose.connect("mongodb://localhost/DTY_recipes");
require("./models/Recette");
require("./models/User");
require("./models/Images");
var app=express();
app.use(cors({origin:"http://localhost:4200"}));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const server = http.Server(app);
const io = socket(server);



function genereLien(nom){
    let s="";
    for(let lettre of nom.toLowerCase()){
        s+=lettre;
        if(lettre=="é" || lettre=="è" || lettre=="ê"){
            s=s.slice(0,-1);
            s+="e";
        }
        if(lettre=="à" || lettre=="â"){
            s=s.slice(0,-1);
            s+="a";
        }
        if(lettre=="ô"){
            s=s.slice(0,-1);
            s+="o";
        }
        if(lettre=="ù"){
            s=s.slice(0,-1);
            s+="o";
        }
        if(lettre=="î"){
            s=s.slice(0,-1);
            s+="i";
        }
        if(lettre==" "){
            s=s.slice(0,-1);
            s+="-";
        }
    }
    return s;
}

function clean(ingredients){
    ingredientsClean=[];
    for(let ingredient of ingredients){
        if(ingredient.nom!="" && ingredient.quantite!=0){
            ingredientsClean.push(ingredient);
        }
    }
    return ingredientsClean;
}

function ressemblance(recette1,recette2){
    console.log(recette1);
    score=0;
    for(let ingredient1 of recette1.ingredients){
        for(let ingredient2 of recette2.ingredients){
            if(genereLien(ingredient1.nom)==genereLien(ingredient2.nom.toLowerCase())){
                score+=1;
            }
        }
    }
    return score;
}

function suggere(recette1,recettes){
    scoreMax=0;
    recetteSuggeree=recettes[0];
    score=0;
    for(let recette2 of recettes){
        score=ressemblance(recette1,recette2);
        if(score>scoreMax && recette2.nom!=recette1.nom){
            score=scoreMax;
            recetteSuggeree=recette2;
        }
    }
    return recetteSuggeree;
}



mongoose.connection.once("open",function(){
    const Recette=mongoose.model("Recette");
    const User=mongoose.model("User");
    const Images=mongoose.model("Images");

    function suggereListe(recettesTestees){
        l=[];
        for(let recette of recettesTestees){
            recetteSuggeree=suggere(recette,recettes);
            if(!recettesTestees.includes(recetteSuggeree) && l.includes(recetteSuggeree)){
                l.push(recetteSuggeree);
            }
        }
        return l;
    }

    app.get("/recettes",function(req,res){
        Recette.find({},{__v:0},function(err,recettes){
            res.send(recettes);
        });

    });

    app.get("/recettes/:recetteUrl",function(req,res){
        Recette.find({url:req.params.recetteUrl },{_id:0,__v:0},function(err,recette){
            res.send(recette[0]);
        });
    });

    app.post("/auth",function(req,res){
        User.findOne({account:req.body.account,password:req.body.password},function(err,user){
            if(user==null){
                res.send({nom:"",logged:false});
            }else{
                Recette.find({},function(err,recettes){
                    res.send({nom:user.account,logged:true,recettesTestees:user.recettesTestees});
                });
            }
        });
    });


    app.post("/addUser",function(req,res1){
        let nom=req.body.account;
        User.findOne({account:nom},function(err,user){
            if(err){
                res.send(err);
            }
            if(user==null){
                User.create({account:nom,
                        password:req.body.password,
                        recettesTestees:[]},function(err,res2){
                    if(err){
                        console.log(err);
                    }else{
                        res1.send({nom:nom,logged:true});
                    }
                });
            }else{
                res.send("existe deja");
            }
        });
    });

    app.post("/testeRecette/:userNom",function(req,res){
        User.findOne({account:req.params.userNom},function(err,user){
            if(err){
                res.send(err);
            }else{
                if(user.recettesTestees.includes(req.body.recetteNom)){
                    res.send(false);
                }else{
                    user.recettesTestees.push(req.body.recetteNom);
                    User.update({_id:user._id},{account:user.account,
                        password:user.password,
                        recettesTestees:user.recettesTestees},function(err){
                            if(!err){
                                res.send(true);
                            }
                        });
                }
            }
        });
    });

    app.post("/ajout",function(req,res){
        let urlReq=genereLien(req.body.nom);
        Recette.findOne({url:urlReq},function(err,recette){
            if(err){
                res.send(err);
            }
            if(recette==null){
                Recette.create({nom:req.body.nom,
                        url:urlReq,
                        difficulte:req.body.difficulte,
                        origine:req.body.origine,
                        ingredients:clean(req.body.ingredients),
                        vegan:req.body.vegan,
                        description:req.body.description,
                        commentaires:[]},function(err,res){
                    if(err){
                        console.log(err);
                    }else{
                    }
                });
            }else{
                res.send("existe deja");
            }
        });
    });



    app.post("/modifie/:recetteUrl",function(req,res){
        Recette.findOne({url:req.params.recetteUrl},function(err,recette){
            Recette.update({_id:recette._id},{nom:recette.nom,
                                            url:req.params.recetteUrl,
                                            difficulte:req.body.difficulte,
                                            origine:req.body.origine,
                                            ingredients:clean(req.body.ingredients),
                                            vegan:req.body.vegan,
                                            description:req.body.description,
                                            commentaires:recette.commentaires},function(err,res){
                if(err){
                    console.log(err);
                }
            });
            res.send(recette);
        });

    });

    app.post("/commente/:recetteUrl",function(req,res1){
        Recette.findOne({url:req.params.recetteUrl},function(err,recette){
            recette.commentaires.push(req.body.commentaire);
                Recette.update({_id:recette._id},{nom:recette.nom,
                    url:recette.url,
                    difficulte:recette.difficulte,
                    origine:recette.origine,
                    ingredients:recette.ingredients,
                    vegan:recette.vegan,
                    description:recette.description,
                    commentaires:recette.commentaires},function(err,res2){
                        if(err){
                            console.log(err);
                        }else{
                            res1.send(recette.commentaires);
                        }
                    }
                );
        });
    });


    var multer = require('multer');
    var upload = multer({ dest: 'Images/' });


    app.post("/addImage/:recetteUrl",upload.single('image'),function(req,res1){
        let imgPath="Images/"+req.params.recetteUrl+".jpg";
        fs.rename("Images/"+req.file.filename,imgPath,function(err){
            if(err){
                console.log(err);
            }else{
                Images.create({url:req.params.recetteUrl,
                    image: {data:fs.readFileSync(imgPath),
                            contentType:"img/jpeg"}},function(err,res2){
                                if(err){
                                    console.log(err);
                                }else{
                                    fs.unlink(imgPath,function(err){});
                                }
                            });
                res1.send(true);
            }
        });
    });

    app.get("/images/:recetteUrl",function(req,res){
        Images.findOne({url:req.params.recetteUrl},function(err,img){
            if(img==null){
                res.send(false);
            }else{
                var base64data = new Buffer(img.image.data, 'binary').toString('base64');
                res.send({image:base64data});
            }
        });
    });

    app.get("/supprime/:recetteUrl",function(req,res){
        Recette.deleteOne({url:req.params.recetteUrl},function(err){
            if(err){
                res.send(err);
            }
        });
    });

});

//app.listen(8080);

server.listen(8080,() => {
    console.log(`App Server Listening at 8080`);
});


